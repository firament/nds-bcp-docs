/*
Database Tables Drop Script for 'Gate Pass Manager'
--
SOURCE LOCATION: $/30-Design/310-DB/3000-SQL/Sprint_01/db-70z-drop-tables.ddl
Version: 0.0.1.0
--
Generated by sak on 2019-12-11 13:50:29
*/
ALTER TABLE DD_ITEMS DROP FOREIGN KEY FKDD_ITEMS184240;
DROP TABLE IF EXISTS A_DB_INFO;
DROP TABLE IF EXISTS DD_CATEGORIES;
DROP TABLE IF EXISTS DD_ITEMS;
DROP TABLE IF EXISTS GEN_APP_SETTINGS;
DROP TABLE IF EXISTS MAINT_LOGIN_HISTORY;
DROP TABLE IF EXISTS MAINT_SESSIONS;
DROP TABLE IF EXISTS MAINT_USER_VERIFY;
DROP TABLE IF EXISTS SYSTEM_LOG_BCP;
DROP TABLE IF EXISTS USERS;
