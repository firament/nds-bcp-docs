# Sprint 1
> Start: 2019-12-09
> Finish: 2019-12-13

## Items
- [x] **1.** Scaffold application
- [x] **2.** Application Scaffold
- [x] **3.** DB Model Scaffold
- [x] **4.** DB Model for Sprint 1
- [x] **5.** Page Templates
- [x] **6.** Application Logging
- [x] **7.** Authentication pipeline
