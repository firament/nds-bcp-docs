# Sprint 2
> Start: 2019-12-16
> Finish: 2019-12-20

## Items
- [x] **1.** DB Model Sprint 2
- [ ] **2.** User Registration w Confirmation
- [x] **3.** Seed Lookups and Masters
- [ ] **4.** Login and Profile Page
- [ ] **5.** RFQ Form
- [ ] **6.** Quote Form
