# technical Debt Items from Sprint 2

- [ ] bcp.Data.dbsets.MaintSessions
	- Make payload serialization transparent to user
	- access only User_Payload
- [ ] Implement UPL.Equals
- [ ] Change 'APPCFG.SessionOpts.AuthCookieName' to 'APPCFG.SessionOpts.AuthKeyName'
- [ ] 
