# Local Commands

## From anywhere
- Open port for mobile testing
    ```sh
    # Open port and do forward
    sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 5020;
    # verify configuration
    sudo iptables -t nat -L
    # get curr addr
    ip addr show
    ```
- Generate Metadata hoisting
> https://github.com/firament/fsap-utils-1706.git
> https://gitlab.com/oblak.blr/vs-trail.git



## From Doc root

## From Src root
- Scaffold DB
> Moved to $/99-scripts/db-scaffold.sh in src for automation

    ```sh
    # Clear earlier generations
    rm -vrf 60-bcp.Data-Scaffold/dbsets/*.cs
    pushd 60-bcp.Data-Scaffold;

    # Using MySql.Data.EntityFrameworkCore - PREFERRED
    dotnet ef dbcontext scaffold \
        "server=127.0.0.1;port=8804;database=BCP_DB;user=bcp-dev-rw;password=cfea457b656147e3" \
        MySql.Data.EntityFrameworkCore \
        -v -f \
        -d \
        -o dbsets \
        -c BCP_DBContext \
        --framework netcoreapp2.2 \
        2>&1 | tee ../../docs/01-Notes/downloads/run-logs/db-scaffold.log

    popd;
    rsync -vhr 60-bcp.Data-Scaffold/dbsets/*.cs 40-bcp.Data/dbsets
    # sync files to constituent project

    ```

- Publish, to update server
> Moved to 99-scripts/publish.sh

    ```sh
    # Clean
    rm -vfr x-out/BCP-Website/*

    # Publish
    DAY_TAG=$(date +"%Y%m%d-%s");
    OUT_DIR="x-out/BCP-Website/R-${DAY_TAG}";
    echo "Building to DIR ${OUT_DIR}";

    mkdir -vp ${OUT_DIR};
    dotnet publish \
        -v n \
        -f netcoreapp3.1 \
        -c Release \
        -o ${PWD}/${OUT_DIR} \
        20-bcp.web/bcp.web.csproj \
        2>&1 | tee ${OUT_DIR}-pub.log

    # Scrub
    rm -vfr ${OUT_DIR}/wwwroot/nlog-output;
    rm -vf ${OUT_DIR}/appsettings.Development.json;
    rm -vf ${OUT_DIR}/NLog.Development.config;

    # Run Test
    pushd ${OUT_DIR};
    dotnet bcp.web.dll --urls "http://*:5020"
    # dotnet bcp.web.dll --urls="http://*:5020;https://*:5030"
    popd;

    # Pack
    pushd ${OUT_DIR}/../
    tar cvzf gpmweb-${DAY_TAG}.tar.gz R-${DAY_TAG}
    echo "Packed file:"
    echo "bcp.web-${DAY_TAG}.tar.gz";
    echo "${PWD}/bcp.web-${DAY_TAG}.tar.gz";
    popd
    ```
