# Parking notes

## 2019 Dec 21
- breaking changes
	- https://docs.microsoft.com/en-us/ef/core/what-is-new/ef-core-3.0/breaking-changes
- nameservers
	- ns1.site4now.net
	- ns2.site4now.net
	- ns3.site4now.net
- CI/CD
	- https://dzone.com/articles/cicd-in-aspnet-mvc-using-jenkins
- Console logging
	- https://andrewlock.net/new-in-aspnetcore-3-structured-logging-for-startup-messages/
- NLog Activity ID
	- https://github.com/NLog/NLog/issues/1984
	- Trace.CorrelationManager.ActivityId is lost when target is wrapped into AsyncTargetWrapper
	- https://www.gitmemory.com/issue/NLog/NLog.Web/386/513304834

## 2019 Dec 24
> dev deployment

- Error
```
HTTP Error 500.31 - ANCM Failed to Find Native Dependencies
Common solutions to this issue:
The specified version of Microsoft.NetCore.App or Microsoft.AspNetCore.App was not found.
Troubleshooting steps:

    Check the system event log for error messages
    Enable logging the application process' stdout messages
    Attach a debugger to the application process and inspect

For more information visit: https://go.microsoft.com/fwlink/?LinkID=2028526 
```

- Solutions
	- https://docs.microsoft.com/en-us/aspnet/core/test/troubleshoot-azure-iis?view=aspnetcore-3.1#50031-ancm-failed-to-find-native-dependencies
		- Install the appropriate version of .NET Core on the machine.
		- Change the app to target a version of .NET Core that's present on the machine.
		- Publish the app as a self-contained deployment.
