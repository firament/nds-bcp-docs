# Date time picker components

## Links
- Bootstrap
	- https://tarruda.github.io/bootstrap-datetimepicker/
	- https://tempusdominus.github.io/bootstrap-4/
	- https://www.malot.fr/bootstrap-datetimepicker/demo.php
	- https://pro.propeller.in/components/datetimepicker.php
- jQuery
- https://www.jqueryscript.net/time-clock/Clean-jQuery-Date-Time-Picker-Plugin-datetimepicker.html

- https://www.thecodedeveloper.com/add-datetimepicker-jquery-plugin/
	- https://github.com/xdan/datetimepicker
	- https://xdsoft.net/jqplugins/datetimepicker/

- Other interesting
	- https://github.com/laazebislam/jTimer
	- 

## flatpickr
> Works
- https://github.com/flatpickr/flatpickr
- https://github.com/flatpickr/flatpickr/archive/master.zip
```
$(".selector").flatpickr({
    enableTime: true,
    minTime: "16:00",
    maxTime: "22:00"
});
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
```
- https://flatpickr.js.org/formatting/


## xdan/datetimepicker
- https://github.com/xdan/datetimepicker
- 

## Tempus Dominus
- x
- CDNJS
	- https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js
	- 

## bootstrap-datetimepicker
- https://tarruda.github.io/bootstrap-datetimepicker/
- To compile/minify you need to have make, node.js and npm on your $PATH
```sh
git clone git://github.com/tarruda/bootstrap-datetimepicker.git
cd bootstrap-datetimepicker
make deps
make build
	# Error
	# Unable to load plugin yui-compress please make sure that it is installed under or at the same level as less
```
- download from https://tarruda.github.io/bootstrap-datetimepicker/assets/dist/bootstrap-datetimepicker-0.0.11.zip
- source https://github.com/tarruda/bootstrap-datetimepicker
- files
	- bootstrap-datetimepicker.min.css
	- bootstrap-datetimepicker.min.js
