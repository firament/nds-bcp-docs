// BCP_Controller _ctor
public ClassName(ILogger<ClassName> _logger, CC.IAppsettings _app_cfg, CC.UserPayload _user_pay_load, CME.IErrorService _err_svc)
: base(_logger, _app_cfg, _user_pay_load, _err_svc)
{
}


| Var Name | Class                                        | Instance Type | Status |
| -------- | -------------------------------------------- | ------------- | :----: |
| log      | ILogger<BCP_Controller>                      | static        |   1    |
| CTX      | bcp.Data.dbsets.BCP_DBContext                | Scoped        |   1    |
| APPCFG   | bcp.Common.IAppsettings                      | Singleton     |   1    |
| ERRLST   | bcp.Common.Constants.ErrorCodeList           | Singleton     |   1    |
| DDHelp   | bcp.Data.Views.DD_Lookup                     | Singleton     |        |
|          |                                              |               |        |
| EF       | bcp.Common.ErrorModels.IErrorCodeFactory     | Scoped        |   ?    |
| ERRVC    | bcp.Common.ErrorModels.IErrorService         | Scoped        |   ?    |
| UPL      | bcp.Common.UserPayload                       | Scoped        |   1    |
|          |                                              |               |        |
| VQPO     | bcp.Biz.Validators.QuotePOValidator          | Val, SN       |   2    |
| VAUTH    | bcp.Biz.Validators.AuthzBizValidators        | Val, SN       |   1    |
|          |                                              |               |        |
| VADM     | bcp.Biz.Validators.AdminBizValidators        | Val, SN       |   1    |
| VAC      | bcp.Biz.Validators.AnchorClientBizValidators | Val, SN       |   1    |
| VCMN     | bcp.Biz.Validators.CommonBizValidators       | Val, SN       |   1    |
| VEML     | bcp.Biz.Validators.EmailBizValidators        | Val, SN       |   1    |
| VLEND    | bcp.Biz.Validators.LenderBizValidators       | Val, SN       |   1    |
| VMSG     | bcp.Biz.Validators.MessagingBizValidators    | Val, SN       |   1    |
| VPLAT    | bcp.Biz.Validators.PlatformBizValidators     | Val, SN       |   1    |
|          |                                              |               |        |
| BQPO     | bcp.Biz.Services.QuotePOService              | Biz, SN       |   2    |
|          |                                              |               |        |
| BADM     | bcp.Biz.Services.AdminBizService             | Biz, SN       |   1    |
| BAC      | bcp.Biz.Services.AnchorClientBizService      | Biz, SN       |   1    |
| BAUTH    | bcp.Biz.Services.AuthzBizService             | Biz, SN       |   1    |
| BCMN     | bcp.Biz.Services.CommonBizService            | Biz, SN       |   1    |
| BEML     | bcp.Biz.Services.EmailBizService             | Biz, SN       |   1    |
| BLEND    | bcp.Biz.Services.LenderBizService            | Biz, SN       |   1    |
| BMSG     | bcp.Biz.Services.MessagingBizService         | Biz, SN       |   1    |
| BPLAT    | bcp.Biz.Services.PlatformBizService          | Biz, SN       |   0    |
|          |                                              |               |        |
| BCGN     | bcp.Biz.Services.CodeGeneratorBizService     | Biz, SN       |   0    |
|          |                                              |               |        |
| DQPO     | bcp.Data.Functions.QuotePOFunctions          | Data, SN      |   2    |
| DAF      | bcp.Data.Functions.AuthzFunctions            |               |        |

// Singleton Services
// Scoped Services
// Validator Services
// Business Services
