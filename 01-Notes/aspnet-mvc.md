# ASP.NET Core 3.1 MVC

## See
- Restart Appln
```cs
private IApplicationLifetime ApplicationLifetime { get; set; }
IApplicationLifetime appLifetime // from DI

ApplicationLifetime = appLifetime;
ApplicationLifetime.StopApplication();
```

- IChangeToken interface
	- https://docs.microsoft.com/en-us/aspnet/core/fundamentals/change-tokens
- Authentication
	- https://docs.microsoft.com/en-us/aspnet/core/security/authentication/cookie?view=aspnetcore-3.1
	- https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.authentication.authenticationhttpcontextextensions?view=aspnetcore-3.1
	- GetTokenAsync
	- AuthenticateAsync
	- SignInAsync
	- SignOutAsync
- Datatype attribute
```cs
[DataType(DataType.Date)]
[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
public DateTime EnrollmentDate { get; set; }

[Display(Name = "Last Name")]
[Required]
[StringLength(50, MinimumLength=2)]
public string LastName { get; set; }

[DisplayFormat(NullDisplayText = "No grade")]
public Grade? Grade { get; set; }
```

## Gen Snippets
- RedirectToRoute
	- `return RedirectToRoute(new { controller = "Home", action = "About" });`

## Formats
- Custom numeric format strings
	- The ";" section separator
	- `string fmt3 = "##;(##);**Zero**";`
	- **One section**    - The format string applies to all values.
	- **Two sections**   - The first section applies to positive values and zeros, and the second section applies to negative values.
	- **Three sections** - The first section applies to positive values, the second section applies to negative values, and the third section applies to zeros.
- Date time formats
	- https://docs.microsoft.com/en-us/dotnet/standard/base-types/custom-date-and-time-format-strings
- Custom TimeSpan format strings
	- https://docs.microsoft.com/en-us/dotnet/standard/base-types/custom-timespan-format-strings
- Enumeration format strings
	- https://docs.microsoft.com/en-us/dotnet/standard/base-types/enumeration-format-strings
	- G or g or F or f
		- Displays the enumeration entry as a string value
	- D or d
		- Displays the enumeration entry as an integer value in the shortest representation possible.
	- X or x
		- Displays the enumeration entry as a hexadecimal value
		- The value is represented with leading zeros as necessary, to ensure that the result string has two characters for each byte in the enumeration type's underlying numeric type
- Composite formatting
	- Format Item Syntax
		- `{ index[,alignment][:formatString]}`
		- Index Component
		- Alignment Component
			- signed integer indicating the preferred formatted field width
			- If the value of alignment is less than the length of the formatted string, alignment is ignored
			- right-aligned if alignment is positive
			- left-aligned if alignment is negative
			-  If padding is necessary, white space is used
		-  Format String Component
			-  A format string that is appropriate for the type of object being formatted
				-  Date and time types
				-  Enumeration types (all types derived from System.Enum)
				-  Numeric types (BigInteger, Byte, Decimal, Double, Int16, Int32, Int64, SByte, Single, UInt16, UInt32, UInt64)
				-  Guid
					-  `N` => `00000000000000000000000000000000`
					-  `D` => `00000000-0000-0000-0000-000000000000`
				-  TimeSpan


## Action Result Types
- https://exceptionnotfound.net/asp-net-core-demystified-action-results/
- Types of Action Results
    - View Result
    - Partial View Result
    - Redirect Result
    - Redirect To Action Result
    - Redirect To Route Result
    - Json Result
    - File Result
    - Content Result
	```cs
	var jsonResult = Json(persons, JsonRequestBehavior.AllowGet);  
	jsonResult.MaxJsonLength = int.MaxValue;  
	return jsonResult;  
	```

	```cs
	return new PhysicalFileResult(_hostingEnvironment.ContentRootPath + "/wwwroot/downloads/pdf-sample.pdf", "application/pdf");
	```

- API notes
	- Microsoft.AspNetCore.Mvc
	- JsonResult(Object, Object)
	- **Parameters**
	- `value`
		- Object 
		- The value to format as JSON.
	- `serializerSettings`
		- Object 
		- The serializer settings to be used by the formatter.
		- When using System.Text.Json, this should be an instance of `JsonSerializerOptions`.
		- When using Newtonsoft.Json, this should be an instance of `JsonSerializerSettings`.

- e.g.
	```cs
	using Microsoft.AspNetCore.Mvc;  
	using StackedChartwithCoreMVCDemo.Models;  
		
	namespace StackedChartwithCoreMVCDemo.Controllers  
	{  
		public class ColumnChartController: Controller  
		{  
			// GET: /<controller>/  
			public IActionResult Index()  
			{  
				return View();  
			}  
			[HttpGet]  
			public JsonResult PopulationChart()  
			{  
				var populationList = PopulationDataAccessaLayer.GetCityPopulationList();  
				return Json(populationList);  
			}  
		}  
	}  

	```


## HTML Helpers
Strongly Typed Helper Method
Html.TextBoxFor(), Html.TextAreaFor(), Html.DropDownListFor(), Html.CheckboxFor(), Html.RadioButtonFor(), Html.ListBoxFor(), Html.PasswordFor(), Html.HiddenFor(), Html.LabelFor()

Template Helper Method
Display, DisplayFor, Editor, and EditorFor

[ DataType ( DataType .MultilineText)]
public string  Address {  get ;  set ; }

Html.EditorFor(m => Model.Address)

### System.Web.Mvc.Html
https://docs.microsoft.com/en-us/dotnet/api/system.web.mvc.html.formextensions?view=aspnet-mvc-5.2

	EditorForModel(HtmlHelper, String, String, Object) 	
	Returns an HTML input element for each property in the model, using the template name, HTML field name, and additional view data.
	
- public static class EnumHelper
	- GetSelectList(Type, Enum)
	- 
			
### 2
if (ModelState.IsValid) { 
    //check whether name is already exists in the database or not
    bool nameAlreadyExists = * check database *       
    if(nameAlreadyExists)
    {
        ModelState.AddModelError(string.Empty, "Student Name already exists.");
        return View(std);
    }
}	

@Html.EditorFor(m => m.StudentName) <br />
@Html.ValidationMessageFor(m => m.StudentName, "", new { @class = "text-danger" })


## Model Binding
- https://docs.microsoft.com/en-us/aspnet/core/mvc/models/model-binding?view=aspnetcore-3.1
```cs
[HttpPost]
public IActionResult OnPost([Bind("LastName,FirstMidName,HireDate")] Instructor instructor)
```
- Dictionaries
		```cs
		public IActionResult OnPost(int? id, Dictionary<int, string> selectedCourses)
		```
	- POST, form or query string data can be one of
		```
		selectedCourses[1050]=Chemistry&selectedCourses[2000]=Economics

		[1050]=Chemistry&selectedCourses[2000]=Economics

		selectedCourses[0].Key=1050&selectedCourses[0].Value=Chemistry&
		selectedCourses[1].Key=2000&selectedCourses[1].Value=Economics

		[0].Key=1050&[0].Value=Chemistry&[1].Key=2000&[1].Value=Economics
		```

- Collections	
	```cs
	public IActionResult OnPost(int? id, int[] selectedCourses)
	```
	- Form or query string data can be in one of the following formats
		```
		selectedCourses=1050&selectedCourses=2000
		selectedCourses[0]=1050&selectedCourses[1]=2000
		[0]=1050&[1]=2000
		selectedCourses[a]=1050&selectedCourses[b]=2000&selectedCourses.index=a&selectedCourses.index=b
		[a]=1050&[b]=2000&index=a&index=b
		// Only for form data
		selectedCourses[]=1050&selectedCourses[]=2000
		```

- IFormFile and IFormFileCollection
